package bar

type (
	BarStruct struct {
		Uno string
		Dos string
	}

	IBar interface {
		GetUno() string
		GetDos() string
	}
)
