package token

type (
	Game struct {
		PlayerID      uint32
		ProductID     string
		GameCode      string
		DataProviders string // DataProviders -- obtenida del microservicio de providers
	}
)
